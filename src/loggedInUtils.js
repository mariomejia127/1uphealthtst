import Cookies from 'js-cookie';

export const getIsLoggedIn = () => {
    const cookies = Cookies.get('access_token');
    return cookies? true: false;
}

export const logOut = () => {
    Cookies.remove('access_token');
}