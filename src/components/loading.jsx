import React, { Component } from 'react';
import {
    Typography,
  } from "@material-ui/core";
class Loading extends Component {
    
    render() { 
        return ( 
            <Typography component="h1" variant="h5">
                {this.props.text }
            </Typography>
         );
    }
}
 
export default Loading;